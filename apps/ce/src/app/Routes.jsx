import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import Root from './components/Root';

export class Routes extends React.Component {
  render() {
    return (
      <Switch>
        <Redirect path='/' to='/cmp' exact />
        <Route path='/cmp' exact component={Root} />
      </Switch>
    );
  }
}
