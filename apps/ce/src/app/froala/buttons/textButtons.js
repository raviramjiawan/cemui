export const textButtons = [
  [
    'fontFamily',
    'fontSize',
    '-',
    'bold',
    'italic',
    'underline',
    'strikeThrough',
    'subscript',
    'superscript',
    '|',
    'backgroundColor',
    'textColor',
  ],
];
