export const paragraphButtons = [
  ['formatOL', 'formatUL', '|', 'outdent', 'indent', '-', 'alignLeft', 'alignCenter', 'alignRight', 'alignJustify'],
  ['paragraphFormat'],
];
