export const richButtons = [
  [
    'insertLink',
    'insertImage',
    'insertVideo',
    'insertTable',
    'emoticons',
    'fontAwesome',
    'specialCharacters',
    'embedly',
    'insertFile',
    'insertHR',
  ],
];
