export const fontConfig = {
  /**
   * Defines the fonts that appear under the Font Family button from the rich text editor's toolbar.
   */
  fontFamily: {
    'Open Sans, sans-serif': 'Open Sans',
    'Ubuntu, sans-serif': 'Ubuntu',
  },
  /**
   * The text to display when the font family is unknown or nothing is selected.
   */
  fontFamilyDefaultSelection: 'Open Sans',
  /**
   * The Font Family button from the WYSIWYG editor's toolbar is replaced with a dropdown
   * showing the actual font family name for the current text selection.
   */
  fontFamilySelection: true,
  /**
   * The Font Size button from the WYSIWYG editor's toolbar is replaced with a dropdown
   * showing the actual font size value for the current text selection.
   */
  fontSizeSelection: true,
  /**
   * Defines the font sizes that appear under the Font Size button from the rich text editor's toolbar.
   */
  fontSize: [8, 10, 12, 14, 18, 24, 36],
  /**
   * The Paragraph Format button from the WYSIWYG editor's toolbar is replaced with a dropdown
   * showing the actual paragraph format name for the current text selection.
   */
  paragraphFormatSelection: true,
};
