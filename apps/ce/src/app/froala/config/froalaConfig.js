// Script to run Froala
import 'froala-editor/css/froala_editor.pkgd.min.css';
import 'froala-editor/js/froala_editor.pkgd.min';

// Languages
import 'froala-editor/js/languages/nl';
import 'froala-editor/js/plugins.pkgd.min';

// Export to PDF
import html2pdf from 'html2pdf.js';
import { miscButtons } from '../buttons/miscButtons';
import { paragraphButtons } from '../buttons/paragraphButtons';
import { richButtons } from '../buttons/richButtons';
import { textButtons } from '../buttons/textButtons';
import { fontConfig } from './fontConfig';
import { pluginConfig } from './pluginConfig';

export const defaultFroalaConfig = {
  attribution: false,
  language: 'nl',
  toolbarContainer: '#toolbarContainer',
  key: 'FSC2H-9G1A17A7A3C2A1rXYf1VPUGRHYZNRJb2JVOOe1HAb2zA3B2A1A1F4F1I1A10B1D7==',
  disableRightClick: true,
  html2pdf,
  toolbarButtons: [...textButtons, ...paragraphButtons, ...richButtons, ...miscButtons],
  ...pluginConfig,
  ...fontConfig,
};
