import Paper from '@mui/material/Paper';
import React, { forwardRef } from 'react';
import FroalaEditor from 'react-froala-wysiwyg';
import FroalaEditorView from 'react-froala-wysiwyg/FroalaEditorView';
import { defaultFroalaConfig } from './config/froalaConfig';
import './froala.module.scss';

const StyledPaper = forwardRef((props, ref) => <Paper {...props} ref={ref} variant="outlined" square />);

export class Froala extends React.Component {
  render() {
    const { model, disabled, onModelChange, tag, config = defaultFroalaConfig } = this.props;

    return disabled ? (
      <FroalaEditorView model={model} tag={tag} />
    ) : (
      <FroalaEditor model={model} onModelChange={onModelChange} config={config} tag={StyledPaper} />
    );
  }
}
