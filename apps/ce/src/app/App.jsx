import { ConnectedRouter } from 'connected-react-router';
import React from 'react';
import { Provider } from 'react-redux';
import MaterialProvider from './material/MaterialProvider';
import { Routes } from './Routes';
import { history } from './store/history';
import { store } from './store/store';

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <MaterialProvider>
          <ConnectedRouter history={history}>
            <Routes />
          </ConnectedRouter>
        </MaterialProvider>
      </Provider>
    );
  }
}

export default App;
