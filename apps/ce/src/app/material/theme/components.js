export const components = {
  MuiGrid: {
    styleOverrides: {
      container: {
        height: '100%',
        overflow: 'hidden',
      },
      item: {
        maxHeight: '100%',
      },
    },
  },
  MuiCollapse: {
    styleOverrides: {
      horizontal: {
        height: 'inherit',
      },
    },
  },
};
