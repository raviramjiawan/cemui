/**
 * @mixin
 */
export const zIndex = {
  /**
   * @type number
   * @default 1000
   */
  mobileStepper: 1000,
  /**
   * @type number
   * @default 1050
   */
  speedDial: 1050,
  /**
   * @type number
   * @default 1100
   */
  appBar: 1100,
  /**
   * @type number
   * @default 1200
   */
  drawer: 1200,
  /**
   * @type number
   * @default 1300
   */
  modal: 1300,
  /**
   * @type number
   * @default 1400
   */
  snackbar: 1400,
  /**
   * @type number
   * @default 1500
   */
  tooltip: 1500,
};
