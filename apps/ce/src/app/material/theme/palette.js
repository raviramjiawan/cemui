export const palette = {
  primary: {
    main: '#2B4279',
  },
  secondary: {
    main: '#EFEAE5',
  },
  info: {
    main: '#0288D1',
  },
  error: {
    main: '#EB5E3C',
  },
  warning: {
    main: '#FFCC33',
  },
  success: {
    main: '#57B46B',
  },
  text: {
    primary: '#151927',
  },
  divider: 'rgba(0, 0, 0, 0.12)',
  background: {
    paper: '#FFF',
    default: '#FFF',
  },
  contrastThreshold: 3,
  tonalOffset: 0.2,
};
