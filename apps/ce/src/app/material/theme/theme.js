import { breakpoints } from './breakpoints';
import { components } from './components';
import { palette } from './palette';
import { shape } from './shape';
import { transitions } from './transitions';
import { typography } from './typography';
import { zIndex } from './zIndex';

export const theme = {
  breakpoints,
  typography,
  palette,
  components,
  zIndex,
  shape,
  transitions,
};
