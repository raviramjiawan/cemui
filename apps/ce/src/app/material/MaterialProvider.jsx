import { LocalizationProvider } from '@mui/lab';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import { createTheme, CssBaseline, responsiveFontSizes, ThemeProvider } from '@mui/material';
import React from 'react';
import { connect } from 'react-redux';
import { MATERIAL_LAB_LOCALE } from './locale/material-lab-locale';
import { MATERIAL_LOCALE } from './locale/material-locale';

class MaterialProvider extends React.Component {
  render() {
    const { theme, locale, children } = this.props;
    return (
      <ThemeProvider theme={responsiveFontSizes(createTheme(theme, MATERIAL_LOCALE[locale]))}>
        <CssBaseline />
        <LocalizationProvider dateAdapter={AdapterDateFns} locale={MATERIAL_LAB_LOCALE[locale]}>
          {children}
        </LocalizationProvider>
      </ThemeProvider>
    );
  }
}

export default connect((state) => ({ theme: state.theme, locale: state.locale }))(MaterialProvider);
