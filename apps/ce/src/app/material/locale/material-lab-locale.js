import { enUS, nl } from 'date-fns/locale';

export const MATERIAL_LAB_LOCALE = {
  en: enUS,
  nl,
};
