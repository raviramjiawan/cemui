import { enUS, nlNL } from '@mui/material/locale';

export const MATERIAL_LOCALE = {
  en: enUS,
  nl: nlNL,
};
