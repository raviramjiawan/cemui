import React from 'react';

class Logo extends React.Component {
  static defaultProps = {
    height: 50,
    alt: 'logo',
  };

  render() {
    return <img src={this.props.src} alt={this.props.alt} height={this.props.height} />;
  }
}

export default Logo;
