import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  Alert,
  Accordion,
  AccordionActions,
  AccordionDetails,
  AccordionSummary,
  Box,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Grid,
  MenuItem,
  Radio,
  RadioGroup,
  Stack,
  Switch,
  TextField,
  Typography,
} from '@mui/material';
import Paper from '@mui/material/Paper';
import React from 'react';
import { Froala } from '../froala/Froala';
import Header from './Header';
import Main from './Main';
import TransitionsModal from './Modal';
import SimpleSnackbar from './Snackbar';
import EnhancedTable from './Table';

const currencies = [
  {
    value: 'USD',
    label: '$',
  },
  {
    value: 'EUR',
    label: '€',
  },
  {
    value: 'BTC',
    label: '฿',
  },
  {
    value: 'JPY',
    label: '¥',
  },
];

class CenterPanel extends React.Component {
  render() {
    return (
      <Stack height="100%">
        <Paper id="toolbarContainer" variant="outlined" square sx={{ borderLeft: 0, borderRight: 0, borderTop: 0 }} />
        <Box maxHeight="100%" overflow="auto">
          <Box margin={2}>
            <Froala />
            <Accordion variant="outlined" defaultExpanded>
              <Grid container alignItems="center">
                <Grid item xs>
                  <AccordionSummary
                    sx={{ flexDirection: 'row-reverse' }}
                    expandIcon={<FontAwesomeIcon icon={{ iconName: 'chevron-down', prefix: 'far' }} />}
                  >
                    <Typography variant="h6" marginLeft={1}>
                      Invoeren profiel gegevens
                    </Typography>
                  </AccordionSummary>
                </Grid>
                <Grid item>
                  <AccordionActions>
                    <FormControlLabel control={<Switch defaultChecked />} label="R" />
                  </AccordionActions>
                </Grid>
              </Grid>
              <AccordionDetails>
                <Grid container spacing={2}>
                  <Grid item xs={6}>
                    <TextField label="Clientcode" InputLabelProps={{ shrink: true }} fullWidth size="small" />
                  </Grid>
                  <Grid item xs={6}>
                    <TextField label="Handelsnaam" InputLabelProps={{ shrink: true }} fullWidth size="small" />
                  </Grid>
                  <Grid item xs={4}>
                    <TextField
                      select
                      label="Rechtsvorm"
                      value="BTC"
                      helperText="Please select your currency"
                      size="small"
                    >
                      {currencies.map((option) => (
                        <MenuItem key={option.value} value={option.value}>
                          {option.label}
                        </MenuItem>
                      ))}
                    </TextField>
                  </Grid>
                </Grid>
              </AccordionDetails>
            </Accordion>
            <Accordion variant="outlined" defaultExpanded>
              <Grid container alignItems="center">
                <Grid item xs>
                  <AccordionSummary
                    sx={{ flexDirection: 'row-reverse' }}
                    expandIcon={<FontAwesomeIcon icon={{ iconName: 'chevron-down', prefix: 'far' }} />}
                  >
                    <Typography variant="h6" marginLeft={1}>
                      Opdracht
                    </Typography>
                  </AccordionSummary>
                </Grid>
                <Grid item>
                  <AccordionActions>
                    <FormControlLabel control={<Switch defaultChecked />} label="R" />
                  </AccordionActions>
                </Grid>
              </Grid>
              <AccordionDetails>
                <Grid container spacing={2}>
                  <Grid item xs={6}>
                    <FormGroup>
                      <FormControlLabel control={<Checkbox name="gilad" />} label="Gilad Gray" />
                      <FormControlLabel control={<Checkbox name="jason" />} label="Jason Killian" />
                      <FormControlLabel control={<Checkbox name="antoine" />} label="Antoine Llorca" />
                    </FormGroup>
                  </Grid>
                  <Grid item xs={6}>
                    <FormControl component="fieldset">
                      <FormLabel component="legend">Gender</FormLabel>
                      <RadioGroup row>
                        <FormControlLabel value="female" control={<Radio />} label="Female" />
                        <FormControlLabel value="male" control={<Radio />} label="Male" />
                        <FormControlLabel value="other" control={<Radio />} label="Other" />
                        <FormControlLabel value="disabled" disabled control={<Radio />} label="other" />
                      </RadioGroup>
                    </FormControl>
                  </Grid>
                  <Grid item xs={4}>
                    <TextField
                      label="Number"
                      InputLabelProps={{ shrink: true }}
                      fullWidth
                      size="small"
                      inputProps={{ type: 'number' }}
                    />
                  </Grid>
                </Grid>
              </AccordionDetails>
            </Accordion>

            <SimpleSnackbar />
            <TransitionsModal />
            <EnhancedTable />

            <Stack spacing={2}>
              <Alert severity="error">This is an error alert — check it out!</Alert>
              <Alert severity="warning">This is a warning alert — check it out!</Alert>
              <Alert severity="info">This is an info alert — check it out!</Alert>
              <Alert severity="success">This is a success alert — check it out!</Alert>
            </Stack>

            <Grid container spacing={1}>
              <Grid item xs={12} sm={4}>
                <Box sx={{ bgcolor: 'primary.main', color: 'primary.contrastText', p: 2 }}>primary.main</Box>
              </Grid>
              <Grid item xs={12} sm={4}>
                <Box
                  sx={{
                    bgcolor: 'secondary.main',
                    color: 'secondary.contrastText',
                    p: 2,
                  }}
                >
                  secondary.main
                </Box>
              </Grid>
              <Grid item xs={12} sm={4}>
                <Box sx={{ bgcolor: 'error.main', color: 'error.contrastText', p: 2 }}>error.main</Box>
              </Grid>
              <Grid item xs={12} sm={4}>
                <Box sx={{ bgcolor: 'warning.main', color: 'warning.contrastText', p: 2 }}>warning.main</Box>
              </Grid>
              <Grid item xs={12} sm={4}>
                <Box sx={{ bgcolor: 'info.main', color: 'info.contrastText', p: 2 }}>info.main</Box>
              </Grid>
              <Grid item xs={12} sm={4}>
                <Box sx={{ bgcolor: 'success.main', color: 'success.contrastText', p: 2 }}>success.main</Box>
              </Grid>
              <Grid item xs={12} sm={4}>
                <Box sx={{ bgcolor: 'text.primary', color: 'background.paper', p: 2 }}>text.primary</Box>
              </Grid>
              <Grid item xs={12} sm={4}>
                <Box sx={{ bgcolor: 'text.secondary', color: 'background.paper', p: 2 }}>text.secondary</Box>
              </Grid>
              <Grid item xs={12} sm={4}>
                <Box sx={{ bgcolor: 'text.disabled', color: 'background.paper', p: 2 }}>text.disabled</Box>
              </Grid>
            </Grid>

            <Typography variant="h1" component="div" gutterBottom>
              h1. Heading
            </Typography>
            <Typography variant="h2" gutterBottom component="div">
              h2. Heading
            </Typography>
            <Typography variant="h3" gutterBottom component="div">
              h3. Heading
            </Typography>
            <Typography variant="h4" gutterBottom component="div">
              h4. Heading
            </Typography>
            <Typography variant="h5" gutterBottom component="div">
              h5. Heading
            </Typography>
            <Typography variant="h6" gutterBottom component="div">
              h6. Heading
            </Typography>
            <Typography variant="subtitle1" gutterBottom component="div">
              subtitle1. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur
            </Typography>
            <Typography variant="subtitle2" gutterBottom component="div">
              subtitle2. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur
            </Typography>
            <Typography variant="body1" gutterBottom>
              body1. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur unde suscipit,
              quam beatae rerum inventore consectetur, neque doloribus, cupiditate numquam dignissimos laborum fugiat
              deleniti? Eum quasi quidem quibusdam.
            </Typography>
            <Typography variant="body2" gutterBottom>
              body2. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur unde suscipit,
              quam beatae rerum inventore consectetur, neque doloribus, cupiditate numquam dignissimos laborum fugiat
              deleniti? Eum quasi quidem quibusdam.
            </Typography>
            <Typography variant="button" display="block" gutterBottom>
              button text
            </Typography>
            <Typography variant="caption" display="block" gutterBottom>
              caption text
            </Typography>
            <Typography variant="overline" display="block" gutterBottom>
              overline text
            </Typography>
          </Box>
        </Box>
      </Stack>
    );
  }
}

export default CenterPanel;
