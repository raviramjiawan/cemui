import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  AppBar,
  Avatar,
  Box,
  Divider,
  IconButton,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  MenuList,
  Tab,
  Tabs,
  Toolbar,
} from '@mui/material';
import React from 'react';
import { connect } from 'react-redux';
import { uiActions } from '../store/ui.reducer';
import Logo from './Logo';

const menuItems = [
  { menu: 'MenuLeft', id: 'menuFigures', names: ['Cijfers', 'Figures'], onClick: 'ViewFigures' },
  { menu: 'MenuLeft', id: 'menuForm', names: ['Dossier', 'Form'], onClick: 'LoadForm' },
  /*{menu: "MenuLeft", id: "menuExpressions", names: ["Expressions", "Expressions"], onClick: "ViewExpressions"},*/
  { menu: 'MenuLeft', id: 'menuReport', names: ['Rapport', 'Report'], onClick: 'ViewReport' },
  { menu: 'MenuLeft', id: 'menuOverviews', names: ['Overzichten', 'Overviews'], onClick: 'ViewOverviews' },
  { menu: 'MenuLeft', id: 'menuServices', names: ['Services', 'Services'], onClick: 'ViewServices' },
  { menu: 'MenuLeft', id: 'menuSettingsTop', names: ['Settings', 'Settings'], onClick: 'ViewAllSettings' },
  { menu: 'MenuLeft', id: 'menuTeam', names: ['Team', 'Team'], onClick: 'ViewTeam' },
];

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 'menuFigures',
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event, value) {
    console.clear();
    this.setState((state) => ({ activeTab: value }));
    // const item = menuItems.find((o) => o.id === value);
    // xview.SetMenu(item);
  }

  render() {
    return (
      <AppBar position="sticky" color="inherit" elevation={1}>
        <Toolbar>
          {!this.props.leftDrawer && (
            <IconButton onClick={() => this.props.dispatch(uiActions.setLeftDrawer())}>
              <FontAwesomeIcon icon={{ iconName: 'bars', prefix: 'far' }} fixedWidth />
            </IconButton>
          )}
          <Logo src="/assets/images/logo.svg" />
          <Tabs variant="fullWidth" value={this.state.activeTab} onChange={this.handleChange}>
            {menuItems.map((item) => {
              return <Tab label={item.names[0]} value={item.id} key={item.id} />;
            })}
          </Tabs>
          <Box sx={{ flexGrow: 1 }} />
          <AvatarMenu />
          {!this.props.rightDrawer && (
            <IconButton onClick={() => this.props.dispatch(uiActions.setRightDrawer())}>
              <FontAwesomeIcon icon={{ iconName: 'bars', prefix: 'far' }} fixedWidth />
            </IconButton>
          )}
        </Toolbar>
      </AppBar>
    );
  }
}

const AvatarMenu = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <IconButton onClick={handleClick} size="small" sx={{ ml: 2 }}>
        <Avatar>M</Avatar>
      </IconButton>
      <Menu
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        transformOrigin={{ horizontal: 'right', vertical: 'top' }}
        anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
      >
        <MenuItem>
          <ListItemIcon>
            <FontAwesomeIcon icon={{ iconName: 'user', prefix: 'far' }} />
          </ListItemIcon>
          <ListItemText>Profile</ListItemText>
        </MenuItem>
        <MenuItem>
          <ListItemIcon>
            <FontAwesomeIcon icon={{ iconName: 'cog', prefix: 'far' }} />
          </ListItemIcon>
          <ListItemText>Settings</ListItemText>
        </MenuItem>
        <MenuItem>
          <ListItemIcon>
            <FontAwesomeIcon icon={{ iconName: 'sign-out', prefix: 'far' }} />
          </ListItemIcon>
          <ListItemText>Logout</ListItemText>
        </MenuItem>
      </Menu>
    </>
  );
};

export default connect((state) => ({
  leftDrawer: state.ui.leftDrawer,
  rightDrawer: state.ui.rightDrawer,
}))(Header);
