import { Stack } from '@mui/material';
import React from 'react';
import Header from './Header';
import Main from './Main';

class Root extends React.Component {
  render() {
    return (
      <Stack height="100%">
        <Header />
        <Main />
      </Stack>
    );
  }
}

export default Root;
