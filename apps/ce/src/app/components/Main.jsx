import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { TreeItem, TreeView } from '@mui/lab';
import { Box, Grid, Grow } from '@mui/material';
import React from 'react';
import { connect } from 'react-redux';
import { uiActions } from '../store/ui.reducer';
import CenterPanel from './CenterPanel';
import Panel from './panel/Panel';
import PanelHeader from './panel/PanelHeader';
import PanelNavigation from './panel/PanelNavigation';

const leftNavigation = [
  {
    label: 'Workflow',
    value: 'workflow',
    iconName: 'folder-open',
  },
  {
    label: 'Posten',
    value: 'posten',
    iconName: 'balance-scale',
  },
  {
    label: 'Correcties',
    value: 'correcties',
    iconName: 'book-open',
  },
  {
    label: 'Issues',
    value: 'issues',
    iconName: 'edit',
  },
  {
    label: 'Team',
    value: 'team',
    iconName: 'users',
  },
];

const rightNavigation = [
  {
    label: 'Richtlijnen',
    value: 'richtlijnen',
    iconName: 'info-square',
  },
  {
    label: 'Knowledge',
    value: 'knowledge',
    iconName: 'books',
  },
  {
    label: 'Tools',
    value: 'tools',
    iconName: 'tools',
  },
  {
    label: 'Review',
    value: 'review',
    iconName: 'check-square',
  },
  {
    label: 'Settings',
    value: 'settings',
    iconName: 'cog',
  },
];

const toggleButtonsLeft = [
  {
    value: 'alle',
    label: 'Alle',
  },
  {
    value: 'opstellen',
    label: 'Opstellen',
  },
  {
    value: 'beoordelen',
    label: 'Beoordelen',
  },
  {
    value: 'notities',
    label: 'Notities',
  },
  {
    value: 'taken',
    label: 'Taken',
  },
];

const toggleButtonsRight = [
  {
    value: 'alle',
    label: 'Alle',
  },
  {
    value: 'mle',
    label: 'MLE',
  },
  {
    value: 'cos',
    label: 'COS',
  },
  {
    value: 'nba',
    label: 'NBA',
  },
  {
    value: 'sra',
    label: 'SRA',
  },
];

const treeNodes = [
  {
    id: '1',
    label: 'Planning',
    items: [
      {
        id: '1.A',
        label: 'Profiel',
      },
      {
        id: '1.B',
        label: 'Scope opdracht',
      },
      {
        id: '1.C',
        label: 'Cijfers en posten',
      },
      {
        id: '11.A',
        label: 'Profiel',
      },
      {
        id: '11.B',
        label: 'Scope opdracht',
      },
      {
        id: '11.C',
        label: 'Cijfers en posten',
      },
    ],
  },
  {
    id: '2',
    label: 'Balans activa',
    items: [
      {
        id: '2.A',
        label: 'Immateriele vaste activa',
      },
      {
        id: '2.A.1',
        label: 'Verloopoverzicht IVA',
      },
      {
        id: '2.B',
        label: 'Materiele vaste activa',
      },
      {
        id: '5.A',
        label: 'Immateriele vaste activa',
      },
      {
        id: '26.A.1',
        label: 'Verloopoverzicht IVA',
      },
      {
        id: '27.B',
        label: 'Materiele vaste activa',
      },
    ],
  },
  {
    id: '3',
    label: 'Activa',
    items: [
      {
        id: '3.A',
        label: 'Vaste activa',
      },
      {
        id: '3.A.1',
        label: 'IVA',
      },
      {
        id: '3.B',
        label: 'Materiele activa',
      },
      {
        id: '4.A',
        label: 'Immateriele vaste activa',
      },
      {
        id: '4.A.1',
        label: 'Verloopoverzicht IVA',
      },
      {
        id: '4.B',
        label: 'Materiele vaste activa',
      },
    ],
  },
];

class Main extends React.Component {
  render() {
    return (
      <Grid container>
        <Grid item>
          <Panel open={this.props.leftDrawer}>
            <PanelHeader
              title="Workflow"
              onClose={() => this.props.dispatch(uiActions.setLeftDrawer())}
              buttons={toggleButtonsLeft}
            />
            <Box flexGrow={1} overflow="auto">
              <TreeView
                defaultCollapseIcon={<FontAwesomeIcon icon={{ iconName: 'folder-open', prefix: 'far' }} />}
                defaultExpandIcon={<FontAwesomeIcon icon={{ iconName: 'folder', prefix: 'far' }} />}
                defaultEndIcon={<FontAwesomeIcon icon={{ iconName: 'file', prefix: 'far' }} />}
                defaultExpanded={['1']}
              >
                <RecursiveTreeItem items={treeNodes} />
              </TreeView>
            </Box>
            <PanelNavigation value="workflow" buttons={leftNavigation} />
          </Panel>
        </Grid>
        <Grid item xs>
          <CenterPanel />
        </Grid>
        <Grid item>
          <Panel open={this.props.rightDrawer} anchor="right">
            <PanelHeader
              title="Richtlijnen"
              onClose={() => this.props.dispatch(uiActions.setRightDrawer())}
              buttons={toggleButtonsRight}
            />
            <Box flexGrow={1}>Content</Box>
            <PanelNavigation value="richtlijnen" buttons={rightNavigation} />
          </Panel>
        </Grid>
      </Grid>
    );
  }
}

const RecursiveTreeItem = ({ items }) =>
  items?.map((item) => (
    <TreeItem key={item.id} nodeId={item.id} label={item.label}>
      {item.items?.length && <RecursiveTreeItem items={item.items} />}
    </TreeItem>
  ));

export default connect((state) => ({
  leftDrawer: state.ui.leftDrawer,
  rightDrawer: state.ui.rightDrawer,
}))(Main);
