import {
  BottomNavigation,
  BottomNavigationAction,
  Button,
  Collapse,
  Drawer,
  Grow,
  List,
  ListItemButton,
  ListItemText,
  ListSubheader,
} from '@mui/material';
import produce from 'immer';
import React from 'react';
import { connect } from 'react-redux';
import { uiActions } from '../store/ui.reducer';

class RightPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
    };

    this.setElement = this.setElement.bind(this);
    this.setOpen = this.setOpen.bind(this);
  }

  setElement(element) {
    if (element) {
      console.log(element);
      this.props.dispatch(uiActions.setLeftDrawer(element.getBoundingClientRect().width));
    }
  }

  setOpen() {
    this.setState((state) =>
      produce(state, (draftState) => {
        draftState.open = !draftState.open;
      })
    );
  }

  render() {
    return (
      <Drawer variant="permanent" anchor="right" PaperProps={{ sx: { position: 'static' } }} sx={{ height: '100%' }}>
        <Button onClick={this.setOpen}>+</Button>

        {/*<Grow in={this.state.open} orientation='horizontal'>*/}
        {/*  <List*/}
        {/*    component='nav'*/}
        {/*    aria-labelledby='nested-list-subheader'*/}
        {/*    subheader={*/}
        {/*      <ListSubheader component='div' id='nested-list-subheader'>*/}
        {/*        Nested List Items*/}
        {/*      </ListSubheader>*/}
        {/*    }*/}
        {/*  >*/}
        {/*    <ListItemButton>*/}
        {/*      <ListItemText primary='Sent mail' />*/}
        {/*    </ListItemButton>*/}
        {/*    <ListItemButton>*/}
        {/*      <ListItemText primary='Drafts' />*/}
        {/*    </ListItemButton>*/}
        {/*    <ListItemButton>*/}
        {/*      <ListItemText primary='Inbox' />*/}
        {/*      Icon*/}
        {/*    </ListItemButton>*/}
        {/*    <Collapse in timeout='auto' unmountOnExit>*/}
        {/*      <List component='div' disablePadding>*/}
        {/*        <ListItemButton sx={{ pl: 4 }}>*/}
        {/*          <ListItemText primary='Starred' />*/}
        {/*        </ListItemButton>*/}
        {/*      </List>*/}
        {/*    </Collapse>*/}
        {/*  </List>*/}
        {/*  <BottomNavigation*/}
        {/*    showLabels*/}
        {/*    onChange={(event, newValue) => {*/}
        {/*      console.log(newValue);*/}
        {/*    }}*/}
        {/*  >*/}
        {/*    <BottomNavigationAction label='Recents' />*/}
        {/*    <BottomNavigationAction label='Favorites' />*/}
        {/*    <BottomNavigationAction label='Nearby' />*/}
        {/*  </BottomNavigation>*/}
        {/*</Grow>*/}
      </Drawer>
    );
  }
}

export default connect()(RightPanel);
