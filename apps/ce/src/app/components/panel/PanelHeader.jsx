import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  Box,
  Divider,
  IconButton,
  InputAdornment,
  Stack,
  TextField,
  ToggleButton,
  ToggleButtonGroup,
  Toolbar,
  Typography,
} from '@mui/material';
import React from 'react';
import { connect } from 'react-redux';

class PanelHeader extends React.Component {
  render() {
    const { buttons, onClose, title } = this.props;

    return (
      <Stack divider={<Divider />}>
        <Toolbar variant="dense" disableGutters>
          <IconButton onClick={onClose}>
            <FontAwesomeIcon icon={{ iconName: 'times', prefix: 'far' }} fixedWidth />
          </IconButton>
          <Typography variant="h6">{title}</Typography>
        </Toolbar>
        <Box padding={1}>
          <TextField
            placeholder="Zoeken..."
            fullWidth
            size="small"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <FontAwesomeIcon icon={{ iconName: 'search', prefix: 'far' }} />
                </InputAdornment>
              ),
            }}
          />
        </Box>
        <Box padding={1}>
          <ToggleButtonGroup color="primary" value="alle" exclusive fullWidth size="small" onChange={() => {}}>
            {buttons.map((button) => (
              <ToggleButton key={button.value} value={button.value}>
                {button.label}
              </ToggleButton>
            ))}
          </ToggleButtonGroup>
        </Box>
      </Stack>
    );
  }
}

export default connect((state) => ({
  leftDrawer: state.ui.leftDrawer,
}))(PanelHeader);
