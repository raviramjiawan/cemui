import { Collapse, Divider, Drawer, Stack } from '@mui/material';
import PropTypes from 'prop-types';
import React from 'react';

class Panel extends React.Component {
  static defaultProps = {
    open: true,
  };

  static propTypes = {
    open: PropTypes.bool,
    anchor: PropTypes.string,
  };

  render() {
    const { open, children, anchor } = this.props;
    return (
      <Drawer
        open={open}
        sx={{ height: '100%' }}
        variant="persistent"
        anchor={anchor}
        PaperProps={{ sx: { position: 'static' } }}
      >
        <Collapse in={open} orientation="horizontal">
          <Stack divider={<Divider />} height="inherit">
            {children}
          </Stack>
        </Collapse>
      </Drawer>
    );
  }
}

export default Panel;
