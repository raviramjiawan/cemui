import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { BottomNavigation, BottomNavigationAction, ButtonBase, Tooltip } from '@mui/material';
import React, { forwardRef } from 'react';

class PanelNavigation extends React.Component {
  render() {
    const { buttons, showLabel, ...props } = this.props;
    return (
      <BottomNavigation {...props}>
        {buttons.map((button) => (
          <BottomNavigationButton key={button.value} showLabel={showLabel} {...button} />
        ))}
      </BottomNavigation>
    );
  }
}

const BottomNavigationButton = ({ label, value, iconName, showLabel, ...props }) => (
  <BottomNavigationAction
    label={showLabel ? label : undefined}
    value={value}
    icon={<FontAwesomeIcon icon={{ iconName, prefix: 'far' }} size="lg" />}
    component={TooltipButton}
    sx={{
      minWidth: 'auto'
    }}
    tooltipProps={{
      title: label,
      arrow: true,
    }}
    {...props}
  />
);

const TooltipButton = forwardRef(({ tooltipProps, ...props }, ref) => {
  return (
    <Tooltip {...tooltipProps}>
      <ButtonBase {...props} ref={ref} />
    </Tooltip>
  );
});

export default PanelNavigation;
