import { combineReducers } from '@reduxjs/toolkit';
import { connectRouter } from 'connected-react-router';
import { history } from './history';
import { localeReducer } from './locale.reducer';
import { themeReducer } from './theme.reducer';
import { uiReducer } from './ui.reducer';

const reducer = combineReducers({
  router: connectRouter(history),
  theme: themeReducer,
  locale: localeReducer,
  ui: uiReducer
});

export default reducer;
