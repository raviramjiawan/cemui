import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  lang: 'en',
};

const localeSlice = createSlice({
  name: 'locale',
  initialState,
  reducers: {
    setLang(state, action) {
      state.lang = action.payload;
    },
  },
});

export const localeActions = localeSlice.actions;
export const localeReducer = localeSlice.reducer;
