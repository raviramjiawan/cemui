import { configureStore } from '@reduxjs/toolkit';
import { routerMiddleware } from 'connected-react-router';
import { history } from './history';
import reducer from './reducer';

export const store = configureStore({
  reducer,
  middleware: [routerMiddleware(history)]
});
