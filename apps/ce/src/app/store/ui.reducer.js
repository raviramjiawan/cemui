import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  leftDrawer: true,
  rightDrawer: true,
};

const uiSlice = createSlice({
  name: 'ui',
  initialState,
  reducers: {
    setLeftDrawer(state) {
      state.leftDrawer = !state.leftDrawer;
    },
    setRightDrawer(state) {
      state.rightDrawer = !state.rightDrawer;
    },
  },
});

export const uiActions = uiSlice.actions;
export const uiReducer = uiSlice.reducer;
