import { createSlice } from '@reduxjs/toolkit';
import { theme } from '../material/theme/theme';

const themeSlice = createSlice({
  name: 'theme',
  initialState: theme,
  reducers: {
    setTheme(state, action) {
      state = action.payload;
    }
  }
});

export const themeActions = themeSlice.actions;
export const themeReducer = themeSlice.reducer;
